
angular.module('boomerang.payment', ['boomerang.state-observer', 'boomerang.login-services', 'boomerang.http-ui-util'])
  .controller('PaymentCtrl', function($stateParams, $scope, $timeout, $ionicPopup, $state, StateObserver, LoginService, HttpUiUtil, $ionicHistory) {

    var self = this;
    var checkout;

    LoginService.getBTToken().then(function(res) {
      braintree.setup(res.data.token, 'custom', {
        id: 'bt-payment-form',
        hostedFields: {
          number: {
            selector: "#card-number",
            placeholder: "4111 1111 1111 1111"
          },
          cvv: {
            selector: "#cvv",
            placeholder: "111"
          },
          expirationDate: {
            selector: "#expiration-date",
            placeholder: "MM/YYYY"
          },
          postalCode: {
            selector: "#postal-code",
            placeholder: "11111"
          },
          styles: {
            'input': {
              'font-size': '16px',
              'font-family': 'courier, monospace',
              'font-weight': 'lighter',
              'color': '#ccc'
            },
            ':focus': {
              'color': 'black'
            },
            '.valid': {
              'color': '#8bdda8'
            }
          }
        },
        onReady: function(res) {
          checkout = res;
          $scope.$digest();
        },
        onError: function(err) {
          self.payment_error = err.message;
          $scope.$digest();
        },
        onPaymentMethodReceived: function(res) {
          self.payment_error = '';
          $scope.$digest();
          self.save(res);
        }
      });

    }, function(err) {
      self.payment_error = err.data.message;
    });


    this.save = function(paymentInfo) {
      var data = {
        payment_method_nonce: paymentInfo.nonce,
        card_type: paymentInfo.details.cardType,
        payment_last_two: paymentInfo.details.lastTwo,
        payment_type: paymentInfo.type
      };
      console.log(data);
      HttpUiUtil.sendHTTPRequest({
        method: 'POST',
        url: '/user/update',
        headers: {'Content-Type': 'application/json'}, data: data}, 'Saving Payment')
        .then(function(res) {
          checkout.teardown();
          $state.go(StateObserver.getPreviousState().name, StateObserver.getPreviousParams());
      });
    };

  });
