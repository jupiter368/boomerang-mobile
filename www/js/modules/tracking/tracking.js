angular.module('boomerang.tracking', ['boomerang.beacon-services', 'boomerang.state-observer', 'boomerang.map', 'boomerang.popup-services'])
  .constant('TripStatus', {
    requested: 'REQUESTED',
    transit: 'TRANSIT',
    cancel: 'CANCEL',
    complete: 'COMPLETE'

  })
  .controller('BeaconTrackingCtrl', function(BeaconService, $state, $stateParams, $scope, $ionicHistory, $ionicPopover, TripStatus, StateObserver, PopupService) {
    var self = this;
    this.status = TripStatus.requested;
    this.beacon = angular.fromJson(localStorage.getItem('requested_data'));


    $scope.$on('beacon:accept', function(event, notificationData) {
      self.status = TripStatus.transit;
      self.reason = "";
      $scope.$digest();
    });

    $scope.$on('beacon:cancel', function(event, notificationData) {
      self.reason = notificationData.reason;
      self.status = TripStatus.cancel;
      $scope.$digest();
    });

    $scope.$on('beacon:arrive', function(event, notificationData) {
      self.status = TripStatus.transit; //arrive can use accepted status
      $scope.$digest();
      PopupService.alert('Your Ride', 'Has arrive at the pickup location', $scope.userType); //do this after $digest() seems to work better
    });

    $scope.$on('transaction:complete', function(event, notificationData) {
      self.backToBeaconList()
      $scope.$digest();
    });


    StateObserver.add('app.beacon-tracking', 'beacon-tracking-observer',
      function() {
        self.beacon = angular.fromJson(localStorage.getItem('requested_data'));
      });

    $ionicPopover.fromTemplateUrl('js/modules/tracking/menu.html', {
      scope: $scope
    }).then(function(popover) {
      self.popover = popover;
    });

    this.showAction = function(event) {
      this.popover.show(event);
    };

    this.backToBeaconList = function() {
      localStorage.removeItem('requested_data');
      $ionicHistory.nextViewOptions({
        disableBack: true
      });
      $state.go('app.beacon-list');
      self.status = TripStatus.requested;
    };

    this.cancel = function() {
      self.popover.hide();
      self.beacon = angular.fromJson(localStorage.getItem('requested_data'));
      BeaconService.cancelBeacon(self.beacon.driver, 'passenger request cancel').then(function(res) {
        self.backToBeaconList();
      }, function(err) {
        self.backToBeaconList();
      });
    };
  });
