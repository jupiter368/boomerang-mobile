angular.module('boomerang.job', ['boomerang.beacon-services', 'ngCordova', 'boomerang.setting', 'boomerang.state-observer', 'boomerang.popup-services'])
.constant('JobStatus', {
  start: 'WAITING',
  stop: 'STOP',
  requested: 'REQUESTED',
  cancel: 'CANCEL',
  transit: 'TRANSIT',
  arrive: 'ARRIVE',
  pickup: 'PICKUP',
  dropoff: 'DROPOFF'
})
.controller('JobCtrl', function(BeaconService, $scope, SettingService, JobStatus, $ionicPopover, $ionicActionSheet, $state, StateObserver, PopupService) {
  var self = this;
  var PING_INTERVAL = 60000; //ping every 1 mins
  //console.log('doh');
  StateObserver.add('app.start-job', 'job-observer', function(event, toState, toStateParams, fromState, fromStateParams) {
    if(localStorage.getItem('charge_id')) {//coming from the charge page
      self.stop();
    }
  });

  //$scope.$on('backgroundMode:activate', function() {
  //});
  //$scope.$on('backgroundMode:deactivate', function() {
  //});

  //function beaconWatcher() {
  //  var curLL;
  //  var watcher = BeaconService.watchLocation();
  //  watcher.then(function(position) {
  //      curLL = {
  //        latitude: position.coords.latitude,
  //        longitude: position.coords.longitude
  //      };
  //      return BeaconService.postBeacon(curLL, false);
  //    }, function(err) {
  //      console.error(err);
  //    })
  //    .then(function(res) {
  //      console.error(res);
  //    }, function(err) {
  //      console.error(err);
  //    });
  //  self.watcher = watcher;
  //  return watcher;
  //};


  $scope.$on('SHUTDOWN', function() {
    self.stop();
  });


  //$scope.enabler = true;
  $scope.$on('beacon:request', function(event, notificationData) {

    self.status = JobStatus.requested;
    self.passengerInfo = {
      latitude: notificationData.latitude,
      longitude: notificationData.longitude,
      distance: notificationData.distance,
      firstname: notificationData.firstname,
      phone: notificationData.phone,
      profilepic: notificationData.profilepic,
      requestId: notificationData.requestId
    };
    $scope.$digest();
  });

  $scope.$on('beacon:cancel', function(event, notificationData) {
    self.status = JobStatus.cancel;
    localStorage.removeItem('passenger_info');
    $scope.$digest();
  });



  $ionicPopover.fromTemplateUrl('js/modules/job-post/menu.html', {
    scope: $scope
  }).then(function(popover) {
    self.popover = popover;
  });
  this.showAction = function(event) {
    this.popover.show(event);
  };

  function beaconTimer(backgroundMode) {
    BeaconService.getCurrentLocation({enableHighAccuracy:false}).then(function(position) {
        var data = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
        };
        return BeaconService.postBeacon(data, false);
      })
      .then(function(res) {
        var tmp;
        if(self.timer || self.backgroundTimer) {
          tmp = setTimeout(beaconTimer.bind(null, backgroundMode), PING_INTERVAL);
        }
        if(backgroundMode) {
          self.backgroundTimer = tmp;
        }
        else {
          self.timer = tmp;
        }
      }, function(err) {
        if(self.timer || self.backgroundTimer) {
          tmp = setTimeout(beaconTimer.bind(null, backgroundMode), PING_INTERVAL);
        }
        if(backgroundMode) {
          self.backgroundTimer = tmp;
        }
        else {
          self.timer = tmp;
        }
      });
  };



  this.init = function() {
    this.status = JobStatus.stop;
  };

  this.start = function(restart) {
    var data;
    BeaconService.getCurrentLocation(true).then(function(position) {
        data = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          serviceradius: self.input.radius,
          servicerate: self.input.price,
          servicearea: self.input.heading,
          reopen: restart
        };
        return BeaconService.postBeacon(data, true);
      }, function(err) {
        self.status = JobStatus.stop;
      })
      .then(function(res) {
        if(!self.watcher && !self.timer) {
          //self.watcher = beaconWatcher();
          self.timer = setTimeout(beaconTimer.bind(null, false), PING_INTERVAL);
        }
        self.status = JobStatus.start;

        //check on start status for state information
        if(res.data.status === 'transit') {
          self.passengerInfo = angular.fromJson(localStorage.getItem('passenger_info'));
          self.status = JobStatus.transit;
          self.input.price = res.data.servicerate;
        }
        if(res.data.status !== 'open') {
          PopupService.httpError(res);
        }

        //if(window.cordova) {
        //  cordova.plugins.backgroundMode.enable();
        //}
      }, function(err) {
        self.status = JobStatus.stop;
      });
  };

  this.stop = function() {
    localStorage.removeItem('passenger_info');
    localStorage.removeItem('charge_id');
    self.passengerInfo = undefined;
    //self.watcher.clearWatch();
    //self.watcher = undefined;
    clearTimeout(self.timer);
    self.timer = undefined;

    //if(window.cordova) {
    //  cordova.plugins.backgroundMode.disable();
    //}

    if(self.status !== JobStatus.stop) {
      BeaconService.deleteBeacon().then();
    }
    self.status = JobStatus.stop;
  };

  this.locatePickupOnMap = function() {
    var url;
    if (ionic.Platform.isIOS()) {
      url = "http://maps.apple.com/?q=taco&ll="+self.passengerInfo.latitude+","+self.passengerInfo.longitude+"&near="+self.passengerInfo.latitude+","+self.passengerInfo.longitude;
    }
    else {
      url = "geo:"+self.passengerInfo.latitude+","+self.passengerInfo.longitude+"?q="+self.passengerInfo.latitude+","+self.passengerInfo.longitude;
    }
    cordova.InAppBrowser.open(url, '_system', 'location=yes');
  }

  this.navigateToPickup = function() {
    var url = "google.navigation:q="+self.passengerInfo.latitude+","+self.passengerInfo.longitude;
    cordova.InAppBrowser.open(url, '_system', 'location=yes');
  }

  this.accept = function() {
    BeaconService.acceptBeacon().then(function(res) {
      self.status = JobStatus.transit;
      localStorage.setItem('passenger_info', angular.toJson(self.passengerInfo));
    });
  }

  this.arrive = function() {
    BeaconService.getCurrentLocation(true).then(function(position) {
      var query = {latitude: position.coords.latitude, longitude: position.coords.longitude};
      return BeaconService.arrive(query);
    }).then(function(res) {
      self.status = JobStatus.arrive;
    });
  }

  this.validate = function(validOptions) {
    if(angular.isArray(validOptions)) {
      return validOptions.indexOf(this.status) > -1;
    }
    else {
      return (this.status === validOptions);
    }
  };

  this.showCancel = function() {
    var buttons = [
      { text: 'Passenger No Show' },
      { text: 'Passenger Cancelled'},
      { text: 'Other Reason'}
    ];
    this.popover.hide();
    var hideSheet = $ionicActionSheet.show({
      buttons: buttons,
      titleText: 'Give a Reason',
      cancelText: "Don't Cancel Trip",
      buttonClicked: function(index) {
        BeaconService.cancelBeacon('_self', buttons[index].text).then(function() {
          localStorage.removeItem('passenger_info');
          self.start(true);
        });
        return true;
      }
    });
  }

  this.clockoff = function() {
    self.popover.hide();
    self.stop();
  }

  this.pickup = function() {
    BeaconService.getCurrentLocation(true).then(function(position) {
        self.passengerInfo.servicestarttime = Date.now();
        return BeaconService.pickup({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          time: self.passengerInfo.servicestarttime
        });
      })
    .then(function(res) {
      self.status = JobStatus.pickup;
    });
  }

  this.dropoff = function() {
    this.passengerInfo.servicestoptime = Date.now();
    this.passengerInfo.fare = this.input.price;
    BeaconService.getCurrentLocation(true).then(function(position) {
      self.passengerInfo.dropofflat = position.coords.latitude;
      self.passengerInfo.dropofflng = position.coords.longitude;
      $state.go('app.charge', self.passengerInfo);
    });
  }

  this.init();
});
