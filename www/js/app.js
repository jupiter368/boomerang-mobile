// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
angular.module('boomerang', [
  'ionic',
  'boomerang.job',
  'boomerang.beacon',
  'boomerang.tracking',
  'boomerang.map',
  'boomerang.payment',
  'boomerang.setting',
  'boomerang.account',
  'boomerang.misc-directives',
  'boomerang.login-services',
  'boomerang.push-notification-services',
  'boomerang.state-observer',
  'boomerang.transaction',
  'boomerang.popup-services',
  'angular-jwt',
  'ngCordova',
  'ionic.rating'])

.run(function($ionicPlatform, $state, $rootScope, $cordovaTouchID, StateObserver) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins) {
      if(window.cordova.plugins.Keyboard) {
        //cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
      }
      if(cordova.plugins.backgroundMode) {
        cordova.plugins.backgroundMode.onactivate = function() {
          $rootScope.$broadcast('backgroundMode:activate');
        };
        cordova.plugins.backgroundMode.ondeactivate = function() {
          $rootScope.$broadcast('backgroundMode:deactivate');
        };
        //cordova.plugins.backgroundMode.enable();
      }

    }

    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    // comment out for now until we can figure out why it's failing on android devices, maybe it's not sported
    // Check for TouchId support
    //$cordovaTouchID.checkSupport().then(function() {
    //  $cordovaTouchID.authenticate("TouchID for Boomerang").then(function() {
    //    //alert("The authentication was successful");
    //  }, function(error) {
    //    console.log(JSON.stringify(error));
    //  });
    //});

    // Hide tabs if keyboard is visible
    window.addEventListener('native.keyboardshow', function () {
      //document.querySelector('div.tabs').style.display = 'none';
      //angular.element(document.querySelector('ion-content.has-tabs')).css('bottom', 0);
      document.body.classList.add('keyboard-open');
    });
    window.addEventListener('native.keyboardhide', function () {
      //var tabs = document.querySelectorAll('div.tabs');
      //angular.element(tabs[0]).css('display', '');
      document.body.classList.add('keyboard-open');
    });

    var currentStateName = localStorage.getItem('current_state');
    if(currentStateName && currentStateName.length > 0) {
      $state.go(currentStateName);
    }

    //fire whenever a state is in transition
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
      StateObserver.changeStart(toState.name, event, toState, toParams, fromState, fromParams);
    });

  });
})

.config(function($stateProvider, $urlRouterProvider, $httpProvider, $ionicConfigProvider, jwtInterceptorProvider) {

  // set all tabs to bottom by default (Android defaults to top normally)
  $ionicConfigProvider.tabs.position('bottom');


  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.

  //set default state
  if(!localStorage.getItem('current_state')) {
    $urlRouterProvider.otherwise('/app/home');
  }

  //app menus
  $stateProvider
    .state('app', {
      url: '/app',
      abstract: true,
      templateUrl: 'templates/main-menu.html',
      controller: 'AppCtrl',
      controllerAs: 'AppCtrl'
    })
    .state('app.home', {
      url: '/home',
      onEnter: enterStatePreprocess,
      views: {
        'menuContent': {
          templateUrl: 'templates/home.html'
        }
      }
    })
    .state('app.beacon-list', {
      url:'/beacon',
      onEnter: enterStatePreprocess,
      views: {
        'menuContent': {
          templateUrl: 'js/modules/beacon/beacon-list.html',
          controller: 'BeaconListCtrl',
          controllerAs: 'BeaconListCtrl'
        }
      }
    })
    .state('app.beacon-profile', {
      url: '/beacon/:driver/:firstname/:lastname/:profilepic/:carpic/:servicerate/:distance/:rating/:latitude/:longitude',
      views: {
        'menuContent': {
          templateUrl: 'js/modules/beacon/beacon-profile.html',
          controller: 'BeaconProfileCtrl',
          controllerAs: 'BeaconProfileCtrl'
        }
      }
    })
    .state('app.beacon-tracking', {
      url: '/tracking',
      views: {
        'menuContent': {
          templateUrl: 'js/modules/tracking/tracking.html',
          controller: 'BeaconTrackingCtrl',
          controllerAs: 'BeaconTrackingCtrl'
        }
      }
    })
    .state('app.start-job', {
      url: '/start-job',
      onEnter: enterStatePreprocess,
      views: {
        menuContent: {
          templateUrl: 'js/modules/job-post/job.html',
          controller: 'JobCtrl',
          controllerAs: 'JobCtrl'
        }
      }
    })
    .state('app.charge', {
      url: '/charge/:fare/:servicestarttime/:servicestoptime/:latitude/:longitude/:dropofflat/:dropofflng',
      views: {
        menuContent: {
          templateUrl: 'js/modules/transaction/charge.html',
          controller: 'ChargeCtrl',
          controllerAs: 'ChargeCtrl'
        }
      }
    })
    .state('app.transaction-list', {
      url: '/transaction',
      onEnter: enterStatePreprocess,
      views: {
        menuContent: {
          templateUrl: 'js/modules/transaction/transaction-list.html',
          controller: 'TransactionListCtrl',
          controllerAs: 'TransactionListCtrl'
        }
      }
    })
    .state('app.transaction-profile', {
      url: '/transaction/:id',
      views: {
        menuContent: {
          templateUrl: 'js/modules/transaction/transaction-profile.html',
          controller: 'TransactionProfileCtrl',
          controllerAs: 'TransactionProfileCtrl'
        }
      }
    })

    .state('app.payment', {
      url: '/payment',
      views: {
        menuContent: {
          templateUrl: 'js/modules/payment/payment-update.html',
          controller: 'PaymentCtrl',
          controllerAs: 'PaymentCtrl'
        }
      }
    })
    .state('app.account', {
      url: '/account',
      onEnter: enterStatePreprocess,
      views: {
        'menuContent': {
          templateUrl: 'js/modules/account/account.html',
          controller: 'AccountCtrl',
          controllerAs: 'AccountCtrl'
        }
        //'view1': {
        //  template: '<ion-view view-title="dohn"><div>doh</div></ion-view>'
        //}
      }
    })

    .state('app.setting', {
      url: '/setting',
      views: {
        'menuContent': {
          templateUrl: 'js/modules/app-setting/setting.html',
          controller: 'SettingCtrl',
          controllerAs: 'SettingCtrl'
        }
      }
    });

  jwtInterceptorProvider.tokenGetter = ['config', function(config) {
    // NOTE to Han: using [string].endsWith was giving me an error on android emulator 'undefined is not a function'
    // so i am doing this instead so i can get past this
    if(config.url.substr(config.url.length - 5) === '.html' ||
      config.url.substr(config.url.length - 4) === '.css' ||
      config.url.substr(config.url.length - 3) === '.css' ) {
      return null;
    }
    return localStorage.getItem('token_id');
  }];
  $httpProvider.interceptors.push('jwtInterceptor');

  //enter process call when a successful state is about to load
  function enterStatePreprocess($rootScope, $timeout, $state, LoginService) {
    //need to wrap in timeout to delay until event is register in AppCtrl.
    $timeout(function() {
      $rootScope.$broadcast('VALIDATE_LOGIN', true);
    });
    $state.transition.then(function(toState) {
      if(toState.name.length > 0 && LoginService.isLogin()) {
        localStorage.setItem('current_state', toState.name);
      }
    });
  }

})

.controller('AppCtrl', function($scope, $ionicModal, $state, LoginService, PushNotificationService, $ionicHistory, $ionicTabsDelegate, PopupService, $timeout) {
  var self = this;

  var defaultType = localStorage.getItem('user_type');
  $scope.userType = (defaultType) ? defaultType : "passenger";
  $ionicTabsDelegate.select( ( $scope.userType === "passenger") ? 0 : 1 );
  $scope.isDriver = function() {
    return $scope.userType === 'driver';
  };

  this.changeUser = function(type) {
    localStorage.setItem('user_type', type);
    $scope.userType = type;
  };

  $scope.$on('VALIDATE_LOGIN', function() {
    if(!LoginService.isLogin()) {
      $ionicModal.fromTemplateUrl('templates/login.html', {
        scope: $scope,
        backdropClickToClose: false,
        hardwareBackButtonClose: false
      }).then(function(modal) {
        self.loginModal = modal;
        self.loginModal.show();
      });
    }
  });

  this.showRegister = function() {
    $ionicModal.fromTemplateUrl('templates/register.html', {
      scope: $scope,
      backdropClickToClose: true,
      hardwareBackButtonClose: true
    }).then(function(modal) {
      self.registerModal = modal;
      self.registerModal.show();
    });
  };

  this.register = function() {

    console.log("registering: "+ this.username + " "+this.password+" "+this.fname+" " + this.lname+" "+this.phone+" " + this.email);

  };

  this.doForgot = function() {
    console.log("forgot password ");
    //$state.go('app.register'); // TEMPORARY: navigate away from this.. login modal should pop up
  };

  this.doLogin = function() {
    $ionicHistory.clearCache(); //some bug in ionic, need to call again at login. weird.
    $ionicHistory.clearHistory();

    $scope.loading = true;
    console.log("logging in " + $scope.userType);
    LoginService.loginUser(this.username, this.password, $scope.userType)
      .then(function() {
        self.loginModal.hide();
        self.username = undefined;
        self.password = undefined;
        self.loginMessage = '';
        return PushNotificationService.register();
      }, function(err) {
        if(err.status === 403) {
          self.loginMessage = err.data.message;
        }
        else if(err.status === 401 || err.status === 400) {
          self.loginMessage = 'Invalid login'
        }
        else {
          self.loginMessage = err.status;
        }
      })
      .then(function() {

      }, function(err) {
        console.error(err);
      }).finally(function($ionicLoading) {
        // On both cases hide the loading
        $scope.loading = false;
      });
  };

  this.reset = function() {
    LoginService.logoutUser();
    $ionicHistory.nextViewOptions({
      disableBack: true
    });
    if($state.current.name !== 'app.home') {
      $state.go('app.home');
    }
    else {
      $state.reload();
    }
    $timeout(function () { //need to wrap in timeout otherwise clear methods doesn't work
      $ionicHistory.clearCache();
      $ionicHistory.clearHistory();
    },300);
  };

  this.logout = function() {
    $scope.$broadcast('SHUTDOWN');
    PushNotificationService.unregister().then(function() {
      self.reset();
    }, function(err) {
      self.reset();
    });
  };

  this.closeLogin = function() {
    self.loginModal.hide();
  }

  // REGISTRATION
  this.r_username = '';
  this.r_password = '';
  this.r_confirm_password = '';
  this.r_fname = '';
  this.r_lname = '';
  this.r_phone = '';
  this.r_email = '';
  this.register_error = '';

  this.clear = function() {
    this.r_username = '';
    this.r_password = '';
    this.r_confirm_password = '';
    this.r_fname = '';
    this.r_lname = '';
    //this.r_phone = $filter('phonenumber')(this.phone); //don't know what $filter is, comment out for now due to error
    this.r_phone = '';
    this.r_email = '';
    this.register_error = '';
  };
  this.doRegister = function() {

    var data = {
      username: this.r_username,
      password: this.r_password,
      firstname: this.r_fname,
      lastname: this.r_lname,
      phone: this.r_phone,
      email: this.r_email
    };
    LoginService.register(data).then(function(res) {
      PopupService.alert("Register Success!!", "Email confirmation send to: " + self.r_email, $scope.userType);
      self.registerModal.hide();
      $timeout(function() { //delay to avoid flash red
        self.clear();
      }, 1000);
    }, function(err) {
      self.register_error = err.data.message;
    });

  };
  this.doRegisterCancel = function() {
    this.registerModal.hide();
    $timeout(function() { //delay to avoid flash red
      self.clear();
    }, 1000);
  };


});



