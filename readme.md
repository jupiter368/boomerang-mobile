//**************************************************************
//beacon API
//**************************************************************

//REQUEST: init driver POST <host>/beacon 
{
	"latitude": 30.12, 
	"longitude": -73.14,
	"servicerate": 50, //in dollars
	"serviceradius": 1609, //in meters
	"reopen": false, //optional boolean flag for reopening closed beacons
	"servicearea": "CODE" //a code that maps the area the driver is heading TBD parameter
}
//RESPONSE
{
	"status": "OK/ERROR/IGNORED",
	"service": "beacon",
	"message": "message" //error message, informational message, or empty string
}

//REQUEST: subseqence driver POST <host>/beacon
{
	"latitude": 30.12, 
	"longitude": -73.14	
}
//RESPONSE
{
	"status": "OK/ERROR",
	"service": "beacon",
	"message": "message" //error message, infomational message, or empty string
}

//REQUEST: passenger GET <host>/beacon
{
	"latitude": 30.12, 
	"longitude": -73.14,
	"destination": "CODE" //a code that maps the area passenger is heading TBD parameter	
}
//RESPONSE
{
	"status": "OK/ERROR",
  "beacons": [{
    "latitude": 38.9442148,
    "longitude": -77.3131025,
    "servicerate": 200,                               //price in dollar
    "driver": "567c6962ebbc94c00a7adb7d",             //beacon id for the next request
    "status": "open",                               
    "collectiontime": 1456341225230,
    "serviceradius": 3218,                          
    "firstname": "james",                             
    "lastname": "kim",
    "geozone": "dqcj8",
    "distance": 66                                    //in meters
  }...]
}

//REQUEST: passenger GET <host>/beacon/request/:id (567c6962ebbc94c00a7adb7d)
{
  "latitude": 30.12, 
  "longitude": -73.14	
}
//RESPONSE
{
  "status": "OK/ERROR"
  "service": "beacon",
  "message": "message" //error message, infomational message, or empty string
}

//REQUEST: passenger GET <host>/beacon/cancel/:id (567c6962ebbc94c00a7adb7d)
{}
//RESPONSE
{
  "status": "OK/ERROR"
  "service": "beacon",
  "message": "message" //error message, infomational message, or empty string
}

//REQUEST: driver POST <host>/beacon/accept
{}
//RESPONSE
{
  "status": "OK/ERROR"
  "service": "beacon",
  "message": "message" //error message, infomational message, or empty string
}


//**************************************************************
//account API
//**************************************************************

//REQUEST: user POST <host>/user/sign_up
{  "username": "string",
   "password": "string",
   "firstname": "string",
   "lastname": "string",
   "phone": "string",
   "email": "string"
}
//RESPONSE: HTTP STATUS 201  
{
  "id": "567c6962ebbc94c00a7adb7d"
}

//REQUEST: user GET <host>/user/confirm_email/:id
{}
//RESPONSE: HTTP STATUS 204
{}

//REQUEST: user POST <host>/user/update  (all fields are optional)
{ 
  "firstname": "string",              
   "lastname": "string",
   "phone": "string",
   "email": "string",
   payment_method_nonce: "string"     //Braintree payment method nonce token
}
//RESPONSE: HTTP STATUS 204/202
{}

//REQUEST: user POST <host>/user/upgrade_to_merchant (all fields are required)
{
  "dateOfBirth": "01/01/2001",
  "ssn": "123-23-4567",
  "street": "12345 One street",
  "city": "New York",
  "state": "NY",
  "zip": "100021",
  "account": "123789345435",
  "routing": "130218302",
  "tos": true/false    //a checkbox for user to acknowledge 
}
//RESPONSE: HTTP STATUS 202