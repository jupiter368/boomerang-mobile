angular.module('boomerang.state-observer', [])
  .factory('StateObserver', function($timeout) {

    var stateObserable = {},
      currentState,
      currentParams,
      previousState,
      previousParams;

    return {
      contains: function(state, name) {
        var observers = stateObserable[state];
        return angular.isDefined(observers) && angular.isDefined(observers[name]);
      },
      add: function(state, name, callback) {
        if(!stateObserable[state]) {
          stateObserable[state] = {};
        }
        stateObserable[state][name] = callback;
      }
      ,
      remove: function(state, name) {
        if(stateObserable[state] && stateObserable[state][name]) {
          delete stateObserable[state][name];
          return true;
        }
        return false;
      },
      //when transition at enter of state name (just ENTER)
      changeStart: function(state, event, toState, toParams, fromState, fromParams) {
        currentState = toState;
        currentParams = toParams;
        previousState = fromState;
        previousParams = fromParams;
        var observers = stateObserable[state];
        for(var name in observers) {
          var proxy = observers[name].bind(undefined);
          proxy(event, toState, toParams, fromState, fromParams);
        }
      },
      getCurrentState: function() {
        return currentState;
      },
      getCurrentParams: function() {
        return currentParams;
      },
      getPreviousState: function() {
        return previousState;
      },
      getPreviousParams: function() {
        return previousParams;
      }
    }
  });
