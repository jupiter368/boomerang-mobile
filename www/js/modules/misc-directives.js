angular.module('boomerang.misc-directives', [])
  .directive('showHideContainer', function(){
    return {
      scope: {
      },
      controller: function($scope, $element, $attrs) {
        $scope.show = false;

        $scope.toggleType = function($event){
          $event.stopPropagation();
          $event.preventDefault();

          $scope.show = !$scope.show;

          // Emit event
          $scope.$broadcast("toggle-type", $scope.show);
        };
      },
      templateUrl: 'templates/show-hide-password.html',
      restrict: 'A',
      replace: false,
      transclude: true
    };
  })
  .directive('showHideInput', function(){
    return {
      scope: {
      },
      link: function(scope, element, attrs) {
        // listen to event
        scope.$on("toggle-type", function(event, show){
          var password_input = element[0],
            input_type = password_input.getAttribute('type');

          if(!show)
          {
            password_input.setAttribute('type', 'password');
          }

          if(show)
          {
            password_input.setAttribute('type', 'text');
          }
        });
      },
      require: '^showHideContainer',
      restrict: 'A',
      replace: false,
      transclude: false
    };
  })
  .directive('phonenumberDirective', ['$filter', function($filter) {
    function link(scope, element, attributes) {

      // scope.inputValue is the value of input element used in template
      scope.inputValue = scope.phonenumberModel;

      scope.$watch('inputValue', function(value, oldValue) {

        value = String(value);
        var number = value.replace(/[^0-9]+/g, '');
        scope.phonenumberModel = number;
        scope.inputValue = $filter('phonenumber')(number);
      });
    }

    return {
      link: link,
      restrict: 'E',
      scope: {
        phonenumberModel: '=model',
      },
      template: '<input ng-model="inputValue" type="tel" placeholder="Phone">',
    };
  }])
  .filter('phonenumber', function() {
    /*
     Format phonenumber as: c (xxx) xxx-xxxx
     or as close as possible if phonenumber length is not 10
     if c is not '1' (country code not USA), does not use country code
     */

    return function (number) {
      /*
       @param {Number | String} number - Number that will be formatted as telephone number
       Returns formatted number: (###) ###-####
       if number.length < 4: ###
       else if number.length < 7: (###) ###

       Does not handle country codes that are not '1' (USA)
       */
      if (!number) { return ''; }

      number = String(number);

      // Will return formattedNumber.
      // If phonenumber isn't longer than an area code, just show number
      var formattedNumber = number;

      // if the first character is '1', strip it out and add it back
      var c = (number[0] == '1') ? '1 ' : '';
      number = number[0] == '1' ? number.slice(1) : number;

      // # (###) ###-#### as c (area) front-end
      var area = number.substring(0,3);
      var front = number.substring(3, 6);
      var end = number.substring(6, 10);

      if (front) {
        formattedNumber = (c + "(" + area + ") " + front);
      }
      if (end) {
        formattedNumber += ("-" + end);
      }
      return formattedNumber;
    };
  });


