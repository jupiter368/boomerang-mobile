angular.module('boomerang.beacon-services', ['ngCordova',  'boomerang.http-ui-util', 'boomerang.popup-services'])
  .factory('BeaconService', function($q, $cordovaGeolocation, $ionicLoading, $ionicPlatform, HttpUiUtil, PopupService) {


    return {
      getBeacons: function(query) {
        return HttpUiUtil.sendHTTPRequest({
            method: 'GET',
            url: '/beacon',
            params: query}, 'Looking for drivers');
      },

      postBeacon: function(data, showSplash) {
        var message;
        if(showSplash) {
          message = 'Setting up beacon';
        }
        return HttpUiUtil.sendHTTPRequest({
          method: 'POST',
          url: '/beacon',
          headers: {'Content-Type': 'application/json'},
          data: data}, message);
      },

      deleteBeacon: function() {
        return HttpUiUtil.sendHTTPRequest({
          method: 'DELETE',
          url: '/beacon',
          headers: {'Content-Type': 'application/json'}
        }, 'Deleting beacon');
      },

      requestBeacon: function(id, query) {
        return HttpUiUtil.sendHTTPRequest({
          method: 'GET',
          url: '/beacon/request/' + id,
          headers: {'Content-Type': 'application/json'},
          params: query
        }, 'Requesting driver');
      },

      cancelBeacon: function(id, reason) {
        return HttpUiUtil.sendHTTPRequest({
          method: 'POST',
          url: '/beacon/cancel/' + id,
          headers: {'Content-Type': 'application/json'},
          data: {reason: reason}
        }, 'Canceling request');
      },

      acceptBeacon: function() {
        return HttpUiUtil.sendHTTPRequest({
          method: 'GET',
          url: '/beacon/accept',
          headers: {'Content-Type': 'application/json'}
        }, 'Accepting request');
      },

      arrive: function(query) {
        return HttpUiUtil.sendHTTPRequest({
          method: 'GET',
          url: '/beacon/arrive',
          headers: {'Content-Type': 'application/json'},
          params: query
        }, 'Notifying passenger');
      },
      pickup: function(query) {
        return HttpUiUtil.sendHTTPRequest({
          method: 'GET',
          url: '/beacon/pickup',
          headers: {'Content-Type': 'application/json'},
          params: query
        });
      },
      getCurrentLocation: function(options, showSplash) {
        var posOptions = {
          enableHighAccuracy: true,
          timeout: 10000

      };
        if(angular.isObject(options)) {
          angular.extend(posOptions, options);
        }
        else {
          showSplash = options;
        }

        var deferred = $q.defer();
        $ionicPlatform.ready(function() {
          if(showSplash) {
            $ionicLoading.show({template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Acquiring location!'});
          }
          $cordovaGeolocation.getCurrentPosition(posOptions)
            .then(function(position) {
              $ionicLoading.hide();
              deferred.resolve(position);
            }, function(err) {
              $ionicLoading.hide();
              deferred.reject(err);
              PopupService.locationError(err);
            });
        });
        return deferred.promise;
      },

      watchLocation: function() {
        var watchOptions = {
          enableHighAccuracy: false, // may cause errors if true
          timeout : 3000
        };
        var watcher = $cordovaGeolocation.watchPosition(watchOptions);
        return watcher;
      }
    }
  });
