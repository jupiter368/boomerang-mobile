angular.module('boomerang.setting', [])
  .factory('SettingService', function() {

    var savedSettings = localStorage.getItem('app_settings');
    savedSettings = (savedSettings) ? angular.fromJson(savedSettings) : {};

    var localSettings = {
        vibrate: true,
        sound: true
      };
    angular.extend(localSettings, savedSettings);

    return {
      getSettings: function() {
        return localSettings;
      },
      setSettings: function(delta) {
        if(delta && Object.keys(delta).length > 0) {
          angular.extend(localSettings, delta);
          angular.extend(savedSettings, delta);
          localStorage.setItem('app_settings', angular.toJson(savedSettings));
          return true;
        }
        else {
          return false;
        }
      }
    }
  })
  .controller('SettingCtrl', function($scope, SettingService) {
    this.vibrate = SettingService.getSettings().vibrate;
    this.sound = SettingService.getSettings().sound;

    $scope.$watch('SettingCtrl', function(newVal, oldVal) {
      var keys = Object.keys(newVal);
      var delta = {};
      angular.forEach(keys, function(key) {
        if(angular.isDefined(newVal[key]) && newVal[key] !== oldVal[key]) {
          delta[key] = newVal[key];
        }
      });
      SettingService.setSettings(delta);
    }, true);
  });
