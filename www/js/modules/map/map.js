angular.module('boomerang.map', ['boomerang.beacon-services'])
  .constant('zoomScale', {
    20 : 1128.497220,
    19 : 2256.994440,
    18 : 4513.988880,
    17 : 9027.977761,
    16 : 18055.955520,
    15 : 36111.911040,
    14 : 72223.822090,
    13 : 144447.644200,
    12 : 288895.288400,
    11 : 577790.576700,
    10 : 1155581.153000,
    9  : 2311162.307000,
    8  : 4622324.614000,
    7  : 9244649.227000,
    6  : 18489298.450000,
    5  : 36978596.910000,
    4  : 73957193.820000,
    3  : 147914387.600000,
    2  : 295828775.300000,
    1  : 591657550.500000
  })
  .factory('CoordinateCalculator', function() {

  })
  .directive('bmMap', function() {
    return {
      require: 'ngModel',
      controller: 'MapCtrl',
      controllerAs: 'MapCtrl',
      template: '<div id="map" style="height: 100%"></div>',
      link: function(scope, element, attrs, ngModelCtrl) {
        scope.MapCtrl.init(ngModelCtrl)
      }
    }
  })
  .controller('MapCtrl', function(BeaconService, $scope, $ionicLoading, $cordovaGeolocation, $element) {
    var self = this;
    var ngModelCtrl;
    var map, myMarker, beaconMarkers=[];
    var zoomLevel = 16;

    ngModelCtrl = {
      $setViewValue: angular.noop
    };

    this.init = function(_ngModelCtrl) {

      ngModelCtrl = _ngModelCtrl;
      ngModelCtrl.$render = renderCB; //need to override %render when using ngModel

      //not sure what this overrid suppose to do
      ngModelCtrl.$validators['validCoordinate'] = function(modelValue, viewValue) {
        var data = modelValue || viewValue;
        return angular.isArray(data) || (data.latitude && data.longitude);
      }

      map = new google.maps.Map($element[0].querySelector('#map'), {
        zoom: zoomLevel,
        //maxZoom: zoomLevel,
        //minZoom: zoomLevel,
        //disableDefaultUI: true,
        //draggable: false,
        //scrollwheel: false,
        //panControl: false
      });

    };
    //function initMap() { //delay initialization
    //  if(!map) {
    //
    //  }
    //};

    function setMyMarker() {
      if(myMarker) {
        myMarker.setMap(null);
      }
      BeaconService.getCurrentLocation({enableHighAccuracy: false}, true).then(function (position) {
        var latlng = {lat: position.coords.latitude, lng:  position.coords.longitude};
        myMarker = new google.maps.Marker({
          animation: google.maps.Animation.DROP,
          position: latlng,
          icon: {
            url: 'img/passenger-pin.png',
            scaledSize: new google.maps.Size(22, 40)
          }

        });
        myMarker.setMap(map);
        map.setCenter(latlng);
      });
    };

    function setBeacons(beacons) {
      for(var i = 0; i < beaconMarkers.length; i++) {
        beaconMarkers[i].setMap(null);
      }
      beaconMarkers = [];
      for(var i = 0; i < beacons.length; i++) {
        beaconMarkers.push(setBeaconMarker(beacons[i]));
      }
    }
    function setBeaconMarker(beacon) {
      var marker;
      if(beacon && beacon.latitude && beacon.longitude) {

        var latlng = {lat: parseFloat(beacon.latitude), lng:  parseFloat(beacon.longitude)};
        marker = new google.maps.Marker({
          animation: google.maps.Animation.DROP,
          position: latlng,
          icon: {
            url: 'img/driver-pin.png',
            scaledSize: new google.maps.Size(22, 40)
          }
        });
        marker.setMap(map);
      }
      return marker;
    }

    //call whenever the $viewValue and $modelValue is different
    //act as change in values event from outside world
    function renderCB() {
      self.beacons = angular.isArray(ngModelCtrl.$viewValue) ? ngModelCtrl.$viewValue : [ngModelCtrl.$viewValue];
      setMyMarker();
      setBeacons(self.beacons);
      return self.beacons;
    };
  });
