angular.module('boomerang.transaction', ['boomerang.transaction-services', 'boomerang.popup-services'])

  .constant('ChargeStatus', {
    success: 'SUCCESS',
    failed: 'FAILED'
  })
  .controller('ChargeCtrl', function($scope, $state, $stateParams, TransactionService, ChargeStatus, $ionicHistory, PopupService) {
    console.log($scope.userType);
    var self = this;
    var currencyFormatter = new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'USD',
      minimumFractionDigits: 2,
    });

    var info = {};
    info.fare = parseFloat($stateParams.fare);
    info.fee = (info.fare * 0.029 + .3);
    info.tax = (info.fare * 0.0875);
    info.servicestarttime = $stateParams.servicestarttime;
    info.servicestoptime = $stateParams.servicestoptime;
    info.source = {latitude: $stateParams.latitude, longitude: $stateParams.longitude};
    info.destination = {latitude: $stateParams.dropofflat, longitude: $stateParams.dropofflng};
    this.tripInfo = info;

    this.dateTimeFormat = function(time) {
      time = parseInt(time);
      return moment(time).format('MMMM Do YYYY, h:mm a');
    }

    this.currencyFormat = function(amount) {
      amount = parseFloat(amount);
      return currencyFormatter.format(amount);
    }
    this.calculateTotal = function() {
      return info.fare + info.tax + info.fee;
    }
    this.charge = function() {
      TransactionService.charge(this.tripInfo).then(function(res) {
        self.status = ChargeStatus.success;
        self.chargeid = res.data.id;
        localStorage.setItem('charge_id', res.data.id);
      }, function(err) {
        self.status = ChargeStatus.failed;
        self.chargemessage = err.data.message;
      }).then(function(res) {

      });
    }
    this.returnToJob = function() {
      $ionicHistory.nextViewOptions({
        disableBack: true
      });
      $state.go('app.start-job');
    }
  })
  .controller('TransactionListCtrl', function($scope, TransactionService, $ionicPopover) {
    var self = this;
    this.transactions = [];
    var page = 0, limit = 13, lastTotalCount = 0, lastReturnCount = -1;



    function fetch(params, broadcastString, append) {
      TransactionService.getAll(params).then(function(res) {
        lastTotalCount = res.data.total_matched;
        lastReturnCount = res.data.total_returned;
        if(append && lastReturnCount > 0) {
          self.transactions.push.apply(self.transactions, res.data.result_set);
        }
        else if(!append && lastReturnCount > 0)  {
          self.transactions = res.data.result_set;
        }
      }, function(err) {
        lastTotalCount = 0;
        lastReturnCount = 0;
      }).finally(function() {
        console.log('page: ' + page);
        console.log('lastTotalCount: ' + lastTotalCount);
        console.log('lastReturnCount: ' + lastReturnCount);
        $scope.$broadcast(broadcastString);

      });
    };

    this.doRefresh = function() {
      var params = {
        limit: limit,
        'order._id': -1, //reverse sort
        extended_fetch: 1,
        offset: 0
      };
      fetch(params, 'scroll.refreshComplete', false);
      page = 0;
    };

    //use -1 as marker to start the first fetch
    this.moreDataCanBeLoaded = function() {
      return lastReturnCount === -1 || lastReturnCount === limit;
    };

    this.loadMore = function() {
      var params = {
        limit: limit,
        'order._id': -1, //reverse sort
        extended_fetch: 1,
        offset: page * limit
      };
      fetch(params, 'scroll.infiniteScrollComplete', true);
      page++;
    };

    $ionicPopover.fromTemplateUrl('js/modules/transaction/menu.html', {
      scope: $scope
    }).then(function(popover) {
      self.popover = popover;
    });

    this.showMenu = function(event) {
      this.popover.show(event);
    }
  })
  .controller('TransactionProfileCtrl', function($state, $stateParams) {
    this.ttr = $stateParams;
  });
