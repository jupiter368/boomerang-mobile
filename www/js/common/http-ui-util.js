angular.module('boomerang.http-ui-util', ['boomerang.login-services', 'boomerang.popup-services'])
  .factory('HttpUiUtil', function($q, $http, $ionicLoading, LoginService, PopupService) {
    return {
      sendHTTPRequest: function(config, loadingMessage) {
        config.url = LoginService.getHostUrl() + config.url;
        if(loadingMessage) {
          $ionicLoading.show({template: '<ion-spinner icon="bubbles"></ion-spinner><br/>'+loadingMessage});
        }
        var deferred = $q.defer();
        $http(config).then(function (res) {
          $ionicLoading.hide();
          deferred.resolve(res);
        }, function(err) {
          $ionicLoading.hide();
          deferred.reject(err);
          PopupService.httpError(err);
        });
        return deferred.promise;
      }
    }
  });
