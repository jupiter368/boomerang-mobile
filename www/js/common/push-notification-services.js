angular.module('boomerang.push-notification-services', ['ngCordova', 'boomerang.login-services', 'boomerang.setting', 'boomerang.popup-services'])
  .factory('PushNotificationService', function($cordovaPushV5, $cordovaVibration, $rootScope, $q, $http, LoginService, $timeout, PopupService) {

    var config = {
      "android": { "senderID": "229604941692" , "icon": "phonegap"},
      "ios": { "alert": true, "badge": true, "sound": true }
    };
    var initialized = false;
    var pushHandle;

    $rootScope.$on('$cordovaPushV5:notificationReceived', function (event, notification) {
      var data = notification.additionalData;
      console.log(notification);
      if(data && typeof data.eventType === 'string') {
        $rootScope.$broadcast(data.eventType, data);
      }
      $timeout(function() { //iOS needs to call finish within 30s. otherwise it will kill the app
        pushHandle.finish();
      }, 10000);

    });
    $rootScope.$on('$cordovaPushV5:errorOccurred', function (event, error) {
      Popup.error(error);
    });

    //app starting
    //token is already send to the server with login
    //don't need to call register
    if((typeof PushNotification) !== 'undefined' && LoginService.isLogin()) {
      $cordovaPushV5.initialize(config).then(function(push) {
        pushHandle = push;
      });;
      $cordovaPushV5.onNotification();
      $cordovaPushV5.onError();
      initialized = true;
    }

    function getDeviceURI() {
      return LoginService.getHostUrl() + '/device/' + device.uuid;
    };

    return {
      register: function() {
        var deferred = $q.defer();

        //if app is running and logout
        if(typeof device === 'undefined' || device.isVirtual) {
          deferred.resolve('not running on device');
        }
        else if((typeof PushNotification) !== 'undefined' && !initialized) {
          $cordovaPushV5.initialize(config).then(function(push) {
            pushHandle = push;
          });
          $cordovaPushV5.onNotification();
          $cordovaPushV5.onError();
          $cordovaPushV5.register().then(function(pushToken) {
            return $http.post(getDeviceURI(),
              {token: pushToken},
              {headers: {'Content-Type': 'application/json'}});
          }, function(err) {
            deferred.reject(err);
          })
          .then(function() {
            initialized = true;
            deferred.resolve("success");
          }, function(err) {
            deferred.reject(err);
          });
        }
        else {
          deferred.reject(new Error('register already called, unregister first before reregistering again'));
        }
        return deferred.promise;
      },

      unregister: function() {
        var deferred = $q.defer();

        if(typeof device === 'undefined' || device.isVirtual) {
          deferred.resolve(true);
        }
        else {
          $http.delete(getDeviceURI())
            .then(function() {
              return $cordovaPushV5.unregister();
            }, function(err) {
              deferred.reject(err);
            })
            .then(function(success) {
              initialized = false;
              deferred.resolve(success);
            }, function(err) {
              deferred.reject(err);
            });
        }


        return deferred.promise;
      },

      getPushToken: function() {
        var deferred = $q.defer();
        $http.get(getDeviceURI()).then(function(res) {
          deferred.resolve(res.data.token)
        }, function(err) {
          deferred.reject(err);
        })

        return deferred.promise;
      }
    }
  });
