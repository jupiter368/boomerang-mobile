
angular.module('boomerang.beacon', ['boomerang.beacon-services', 'boomerang.state-observer', 'boomerang.map', 'boomerang.popup-services'])
  .constant('CarType', {
    z4: {type: 'z4', value: '4'},
    z6: {type: 'z6', value: '6'},
    z8: {type: 'z8', value: '8'}})
  .constant('FilterType', {
    price: 0,
    time: 1,
    car: 2
  })

  .constant('ViewingStatus', {
    list: 'LIST',
    map: 'MAP'
  })
.controller('BeaconListCtrl', function(BeaconService, PushNotificationService, CarType, $scope, StateObserver, $state, $ionicHistory, $ionicPopover, ViewingStatus, PopupService) {
  var self = this;

  self.max_price_low = '25';
  self.max_price = '50';
  self.max_price_high = '200';

  self.max_time_low = '5';
  self.max_time = '15';
  self.max_time_high = '60';


  self.car_type = CarType.z4;
  this.beacons = [];
  this.status = ViewingStatus.list;

  $ionicPopover.fromTemplateUrl('js/modules/beacon/beacon-filter-popover.html', {
    scope: $scope
  }).then(function(popover) {
    $scope.popover = popover;
  });
  this.openPopover = function($event, filterType) {
    console.log("filterType: " + filterType);

    // bind variables to popover scope
    $scope.max_price = self.max_price;
    $scope.max_price_high = self.max_price_high;
    $scope.max_price_low = self.max_price_low;
    $scope.max_time = self.max_time;
    $scope.max_time_high = self.max_time_high;
    $scope.max_time_low = self.max_time_low;
    $scope.filterPrice = filterType === 0; //filterType === FilterType.price;
    $scope.filterTime = filterType === 1; //filterType === FilterType.time;
    $scope.filterCar = filterType === 2; //filterType === FilterType.car;


    $scope.CarType = CarType;
    $scope.car_type = self.car_type;
    $scope.popover.show($event);
  };
  $scope.closePopover = function() {
    $scope.popover.hide();
  };
  $scope.$on('$destroy', function() {
    $scope.popover.remove();
  });
  $scope.$on('popover.hidden', function() {
  });
  $scope.$on('popover.removed', function() {
  });
  $scope.updatePrice = function(val) { self.max_price = $scope.max_price = val; console.log("price is " + val);};
  $scope.updateTime = function(val) { self.max_time = $scope.max_time = val; console.log("time is " + val);};
  $scope.updateCar = function(val) { self.car_type = $scope.car_type = val; console.log("car is " + val);};


  //go straight to the tracking page if a request already pending
  function enterBeaconListStateLogic(event, fromState) {
    if(localStorage.getItem('requested_data')) {//switching from another page, check pending request
      if(event) {
        event.preventDefault();
      }
      $ionicHistory.nextViewOptions({
        disableBack: true
      });
      $state.go('app.beacon-tracking');
    }
    if(fromState && fromState.name === 'app.beacon-tracking') {//a cancel event happen from the tracking page
      self.beacons = [];
    }
  }

  StateObserver.add('app.beacon-list', 'beacon-list-observer', function(event, toState, toParams, fromState, fromParams) {
    enterBeaconListStateLogic(event, fromState);
  })


  this.search = function() {
    BeaconService.getCurrentLocation(true)
      .then(function(position) {
          var query = {latitude: position.coords.latitude, longitude: position.coords.longitude};
          return BeaconService.getBeacons(query);
        })
      .then(function(res) {
        self.beacons = res.data.beacons;
      });
  };
  this.setView = function(type) {
    console.log(type);
    this.status = type;
  }
  enterBeaconListStateLogic();
})

.controller('BeaconProfileCtrl', function(BeaconService, $state, $stateParams, $ionicHistory, PopupService) {
  var self = this;
  this.beacon = $stateParams;
  //this.hiderequest =
  //this.beacon.rating = 3;
  this.request = function() {
    BeaconService.getCurrentLocation(true).then(function(position) {
      var query = {latitude: position.coords.latitude, longitude: position.coords.longitude};
      return BeaconService.requestBeacon(self.beacon.driver, query);
    }).then(function(res) {
      localStorage.setItem('requested_data', angular.toJson(self.beacon));
      $ionicHistory.nextViewOptions({
        disableBack: true
      });
      $state.go('app.beacon-tracking');
    });
  };
  });

