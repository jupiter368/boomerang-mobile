angular.module('boomerang.transaction-services', ['boomerang.http-ui-util'])
  .factory('TransactionService', function(HttpUiUtil) {



    return {
      charge: function(values) {
        if(!values.fare || !values.fee || !values.tax ||
          !values.servicestarttime || !values.servicestoptime ||
          !values.source || !values.destination) {
          throw new Error('mising values in create transaction: ' + values);
        }
        return HttpUiUtil.sendHTTPRequest({
          method: 'POST',
          url: '/transaction',
          data: values}, 'charging transaction');
      },

      getAll: function(params) {
        return HttpUiUtil.sendHTTPRequest({
          method: 'GET',
          url: '/transaction',
          params: params});
      },
      update: function(id, values) {
      },
      get: function(id) {
      },
      delete: function(id) {
      }
    }
  });
