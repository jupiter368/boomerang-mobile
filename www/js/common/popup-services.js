angular.module('boomerang.popup-services', ['ionic'])
  .factory('PopupService', function($ionicPopup) {
    return {
      locationError: function(err) {
        return $ionicPopup.alert({
          title: 'location error',
          template: err.toString(),
          okText: 'Dismiss',
          okType: ' button-royal'
        });
      },
      httpError: function(err) {
        return $ionicPopup.alert({
          title: err.status,
          template: angular.toJson(err.data),
          okText: 'Dismiss',
          okType: 'button-assertive'
        });
      },
      alert: function(title, message, buttonClass) {
        return $ionicPopup.alert({
          title: title,
          template: message,
          okType: buttonClass || 'button-dark'
        });
      },
      forgotPassword: function() {
        return $ionicPopup.alert({

        })
      }
    }
  });
