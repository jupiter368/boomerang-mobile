angular.module('boomerang.login-services', ['angular-jwt'])
  .constant('LoginTypeURI', {
    driver: '/user/driver_auth',
    passenger: '/user/auth',
    default: '/user/auth'
  })
  .factory('LoginService', function($q, $http, jwtHelper, LoginTypeURI) {
    var currentUserType = ''; // default to passenger? (using this variable in controllers for getting type of user currently logged in

    return {
      currentUserType: function() {
        return currentUserType;
      },
      getHostUrl: function() {
        //return 'https://192.168.1.106:5000';
        //return 'https://10.64.48.19:5000';
        //return 'https://69.243.9.107:5000';
        return 'https://localhost:5000';
      },
      isLogin: function() {
        return (localStorage.getItem('token_id') !== null);
      },
      loginUser: function(name, pwd, type) {
        var deferred = $q.defer();
        var loginURI = (type && LoginTypeURI[type]) ? LoginTypeURI[type] : LoginTypeURI.default;
        $http.post(this.getHostUrl() + loginURI,
          {username: name, password: pwd},
          {headers: {'Content-Type': 'application/json'}})
        .then(function(res) {
          localStorage.setItem('token_id', res.data.token);
          deferred.resolve(true);
          currentUserType = type;
        }, function(err) {
          deferred.reject(err);
        });
        return deferred.promise;
      },
      logoutUser: function() {
        localStorage.clear();
      },
      getAccount: function() {
        return $http.get(this.getHostUrl() + '/user/account');
      },
      register: function(data) {
        return $http.post(this.getHostUrl() + '/user/sign_up', data, {headers: {'Content-Type': 'application/json'}});
      },
      getBTToken: function() {
        return $http.get(this.getHostUrl() + '/user/bt_client_token', {headers: {'Content-Type': 'application/json'}});
      }
    }
  });

