angular.module('boomerang.account', ['boomerang.login-services' , 'jrCrop', 'boomerang.popup-services', 'boomerang.http-ui-util'])
  .controller('AccountCtrl', function(LoginService, $state, $scope, $location,  $jrCrop, $ionicActionSheet, $ionicPopover, $ionicModal, PopupService, HttpUiUtil) {

    var self = this;
    LoginService.getAccount().then(function(res) {
      self.account = res.data;
      $scope.current_profile_pic = self.account.profilepic = "https://s3.amazonaws.com/uifaces/faces/twitter/k/128.jpg"; // FIXME! this still hard-coded. need to return from API!
      $scope.current_car_pic = self.account.carpic = "img/car-profile.jpg"; // FIXME! this still hard-coded. need to return from API!
    }, function(err) {
      console.log(err.message);
    });

    var destinationType; // sets the format of returned value
    self.clearUpgrade = function() {
      self.driver = {
        dateOfBirth: '',
        ssn: '',
        street: '',
        city: '',
        state: '',
        zip: '',
        tos: false, // terms of service
        account: '1123581321',
        routing: '071101307'
      };
    };
    self.clearUpgrade();

    // on DeviceReady check if already logged in (in our case CODE saved)
    ionic.Platform.ready(function() {
      if (!navigator.camera)
      {
        // error handling
        return;
      }
      destinationType=navigator.camera.DestinationType.FILE_URI;
    });

    // edit existing profile pic
    self.editPicture = function() {
      $jrCrop.crop({
        url: $scope.current_profile_pic,
        width: 300,
        height: 300,
        circle: true
      }).then(function(canvas) {
        // success!
        var image = canvas.toDataURL();
        $scope.current_profile_pic = self.account.profilepic = image;
      }, function() {
        // User canceled or couldn't load image.
      });

    };
    // take picture or choose from photo library
    self.getPicture = function(pictureSource) {
      //console.log("got camera button click");
      var options =   {
        quality: 50,
        destinationType: destinationType,
        sourceType: pictureSource,
        encodingType: 0,
        //allowEdit : true,
        popoverOptions: CameraPopoverOptions,
        correctOrientation: true,
        saveToPhotoAlbum: true
      };
      if (!navigator.camera)
      {
        // error handling
        return;
      }
      navigator.camera.getPicture(
        function (imageURI) {
           //This plugin does editing.. not sure i need it here tho because cam plugin handles editing. commenting out for now.
          $jrCrop.crop({
            url: imageURI,
            width: 300,
            height: 300,
            circle: true
          }).then(function(canvas) {
            // success!
            var image = canvas.toDataURL();
            $scope.current_profile_pic = self.account.profilepic = image;
          }, function() {
            // User canceled or couldn't load image.
          });

        },
        function (err) {
          //console.log("got camera error ", err);
          // error handling camera plugin
        },
        options);
    };

    // do POST on upload url form by http / html form
    $scope.update = function(obj) {

      // ARE WE SAVING TO Amazon S3 storage??

      //if (!self.account.profilepic)
      //{
      //  // error handling no picture given
      //  return;
      //}
      //var options = new FileUploadOptions();
      //options.fileKey="ffile";
      //options.fileName= self.account.profilepic.substr(self.account.profilepice.lastIndexOf('/')+1);
      //options.mimeType="image/jpeg";
      //var params = {};
      //params.other = obj.text; // some other POST fields
      //options.params = params;
      //
      ////console.log("new imp: prepare upload now");
      //var ft = new FileTransfer();
      //ft.upload(self.account.profilepic, encodeURI([[AMAZON_S3_URL]]), uploadSuccess, uploadError, options);
      //function uploadSuccess(r) {
      //  // handle success like a message to the user
      //}
      //function uploadError(error) {
      //  //console.log("upload error source " + error.source);
      //  //console.log("upload error target " + error.target);
      //}
    };

    /**
     * Show action sheet
     */
    this.showPicChoices = function() {
      var buttons = [
        { text: 'Edit Picture' },
        { text: 'Take A New Picture' },
        { text: 'Upload A Picture'}
      ];
      var hideSheet = $ionicActionSheet.show({
        buttons: buttons,
        titleText: 'Update Profile Picture',
        cancelText: "Cancel",
        buttonClicked: function(index) {
          if (index === 0) {  // edit existing profile pic
            self.editPicture();
          } else if (index === 1) { // take a pic
            self.getPicture(navigator.camera.PictureSourceType.CAMERA);
          } else if (index === 2) { //upload existing pic
            self.getPicture(navigator.camera.PictureSourceType.PHOTOLIBRARY);
          }
          hideSheet();
        }
      });
    };

    $ionicPopover.fromTemplateUrl('js/modules/account/menu.html', {
      scope: $scope
    }).then(function(popover) {
      self.popover = popover;
    });

    this.showMenu = function(event) {
      this.popover.show(event);
    }

    this.updatePayment = function() {
      this.popover.hide();
      $state.go('app.payment');
    }

    this.deletePayment = function() {

    }

    this.upgradeCancel = function() {
      self.upgradeModal.hide();
      self.clearUpgrade();
    }

    this.upgradeSignup = function() {
      HttpUiUtil.sendHTTPRequest({
        method: 'POST',
        url: '/user/update_to_merchant',
        headers: {'Content-Type': 'application/json'}, data: self.driver
      }, 'Updating Driver Info')
        .then(function(res) {
          self.upgradeModal.hide();
          self.clearUpgrade();
          PopupService.alert('Update Driver Info', 'Your driver information is waiting for approval');
      });
    }

    this.upgradeToMerchant = function() {
      self.popover.hide();
      $ionicModal.fromTemplateUrl('js/modules/account/upgrade.html', {
        scope: $scope,
        backdropClickToClose: true,
        hardwareBackButtonClose: true
      }).then(function(modal) {
        self.upgradeModal = modal;
        self.upgradeModal.show();
      });
    }
  })

  .directive('bmCard', function() {
    return {
      templateUrl: 'js/modules/account/saved-cards.html',
      require: 'ngModel',
      controller: 'CardCtrl',
      controllerAs: 'CardCtrl',
      link: function(scope, element, attrs, ngModelCtrl) {
        scope.CardCtrl.init(ngModelCtrl);
      }
    }
  })
  .controller('CardCtrl', function() {
    var self = this, ngModelCtrl;
    this.init = function(_ngModelCtrl) {
      ngModelCtrl = _ngModelCtrl;
      ngModelCtrl.$render = renderCB;
    };

    function renderCB() {
      self.payInfo = ngModelCtrl.$viewValue;
      console.log(self.payInfo);
      return self.payInfo;
    }
  });
